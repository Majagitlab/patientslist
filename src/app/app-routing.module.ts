import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ListPatientsComponent } from './list-patients/list-patients.component';
import { HomeComponent } from './home/home.component';
import { AddNewPatientComponent } from './add-new-patient/add-new-patient.component';


const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'list-patients', component: ListPatientsComponent},
  { path: 'add-new-patient', component: AddNewPatientComponent }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
