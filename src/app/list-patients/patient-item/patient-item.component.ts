import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-patient-item',
  templateUrl: './patient-item.component.html',
  styleUrls: ['./patient-item.component.scss']
})
export class PatientItemComponent implements OnInit {
  @Input() patient: any;
  constructor() { }

  ngOnInit(): void {
  }

}
