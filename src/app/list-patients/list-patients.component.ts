import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { MatTableDataSource } from '@angular/material/table';

import { ListPatients } from './list-patients.service';
import { Patient } from '../models/patient';


@Component({
  selector: 'app-list-patients',
  templateUrl: './list-patients.component.html',
  styleUrls: ['./list-patients.component.scss'],
})
export class ListPatientsComponent implements OnInit {
  patientItems = [];
  singlePatient: boolean = false;

  displayedColumns: string[] = [
    'First name',
    'Last name',
    'Registered date',
    'Doctor',
    'Phone',
    'Email',
    'Street',
    'City',
    'Zip',
    'Country',
  ];
  dataSource: any;
  listsPatients: any;

  constructor(private listPatients: ListPatients, private router: Router) {}

  ngOnInit(): void {
    this.getPatientsList();
  }

  getPatientsList() {
    this.listPatients.getProducts().subscribe((data) => {
      this.listsPatients = data;
      console.log(this.listsPatients);
      this.dataSource = new MatTableDataSource(this.listsPatients);
      this.singlePatient = false;
    });
  }

  getPatientDetails(obj) {
    console.log(obj);
    this.patientItems = obj;
    this.singlePatient = true;
  }
}
