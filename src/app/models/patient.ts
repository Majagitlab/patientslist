
export class Patient {
    firstName: string;
      lastName: string;
      birthDate: Date;
      VATcode: number;
      email: string;
      doctors: string;
      address: any;
      phone: number;
      street: string;
      city: string;
      zip: number;
      country: string;

    constructor(id: number, obj: any ) {
        this.firstName = obj.firstName;
        this.lastName = obj.lastName;
        this.birthDate = obj.birthDate;
        this.VATcode = obj.VATcode;
        this.email = obj.email;
        this.doctors = obj.doctors;
        this.address = obj.address;
        this.phone = obj.phone;
        this.street = obj.street;
        this.city = obj.city;
        this.zip = obj.zip;
        this.country = obj.country;
    }
}