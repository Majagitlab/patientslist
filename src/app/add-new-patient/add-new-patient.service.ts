import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AddNewPatientService {
  private url = "http://localhost:3000/";
  
  constructor(private http: HttpClient) { }

  getDoctors() {
    return this.http.get(this.url + 'doctors');
  }

   addNewPatient(obj) {
    return this.http.post(this.url + 'patient', { obj })
  }
}