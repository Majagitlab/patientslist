import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Router } from '@angular/router';
import {
  FormBuilder,
  Validators,
  FormControl,
  FormGroup,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

// services
import { AddNewPatientService } from './add-new-patient.service';

interface selectFrom {
  value: string;
}

@Component({
  selector: 'app-add-new-patient',
  templateUrl: './add-new-patient.component.html',
  styleUrls: ['./add-new-patient.component.scss'],
})
export class AddNewPatientComponent implements OnInit {
  doctorsList: any;
  addNewPatientForm: any;
  submitedValid: boolean = false;
  submitedInvalid: boolean = false;
  type: selectFrom[] = [
    { value: 'Second Home' },
    { value: 'Work' },
    { value: 'Holiday Place' },
    { value: 'Close Relative' },
  ];

  constructor(
    private addNewPatientService: AddNewPatientService,
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private infoBar: MatSnackBar,
    private router: Router
  ) {
    this.addNewPatientForm = this.formBuilder.group({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      birthDate: new FormControl('', Validators.required),
      VATcode: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      doctors: new FormControl('', Validators.required),
      address: new FormGroup({
      phone: new FormControl('', Validators.required),
      street: new FormControl('', Validators.required),
      city: new FormControl('', Validators.required),
      zip: new FormControl('', Validators.required),
      country: new FormControl('', Validators.required),
      }),
    });
  }

  ngOnInit(): void {
    this.getListDoctors();
  }

  // get doctors list
  getListDoctors() {
    this.addNewPatientService.getDoctors().subscribe((data) => {
      this.doctorsList = data;
      console.log(this.doctorsList);
    });
  }
  // Form Validation
  getFirstNameNameErrMsg() {
    return this.addNewPatientForm.controls['firstName'].hasError('required')
      ? 'please enter yur first name'
      : '';
  }
  getLastNameErrMsg() {
    return this.addNewPatientForm.controls['lastName'].hasError('required')
      ? 'Please enter your last name'
      : '';
  }
  getBirthDateErrMsg() {
    return this.addNewPatientForm.controls['birthDate'].hasError('required')
      ? 'You must choose a birth date'
      : '';
  }
  getVatCodeErrMsg() {
    return this.addNewPatientForm.controls['VATcode'].hasError('required')
      ? 'Please enter your VAT code'
      : '';
  }
  getEmailErrMsg() {
    return this.addNewPatientForm.controls['email'].hasError('email')
      ? 'Please enter your email address'
      : '';
  }
  getDoctorsErrMsg() {
    return this.addNewPatientForm.controls['doctors'].hasError('required')
      ? 'You must choose a doctor'
      : '';
  }

  saveNewPatient(obj) {
    this.addNewPatientService.addNewPatient(obj).subscribe(
      () => {
        this.infoBar.open('New Patient Added Successfully!', '', {
          duration: 100,
          verticalPosition: 'top',
          panelClass: ['infobar', 'delete-bar'],
        });
        this.router.navigate(['/']);
        this.addNewPatientForm.reset();
      },
      (err: any) => {
        console.log(err);
        this.submitedValid = false;
        this.infoBar.open(err.error.message, '', {
          duration: 1000,
          verticalPosition: 'top',
          panelClass: ['infobar', 'error-bar'],
        });
      }
    );
  }

  // Close Info Messages on click
  closeMessage() {
    this.submitedValid = false;
    this.submitedInvalid = false;
  }
}
